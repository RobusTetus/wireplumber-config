#/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

if [ `id -u` = 0 ]; then
	INSTALL_DIRECTORY="/etc/wireplumber"
	echo "Uninstalling Wireplumber configs systemwide in $INSTALL_DIRECTORY..."
else
	INSTALL_DIRECTORY="/home/`whoami`/.config/wireplumber"
	echo "Uninstalling Wireplumber configs in $INSTALL_DIRECTORY user directory..."
fi

for i in $INSTALL_DIRECTORY/wireplumber.conf.d/*; do
rm $i
done

if [ `id -u` = 0 ]; then
	echo "Done, please restart Wireplumber manually..."
else
	echo "Done, restarting Wireplumber."
	systemctl --user restart wireplumber.service
fi