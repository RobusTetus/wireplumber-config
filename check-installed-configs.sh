#/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

RED="\e[31m"
GREEN="\e[32m"
YELLOW="\e[1;33m"
RESET="\e[0m"

printf "Checking files in systemwide directory /etc/wireplumber/wireplumber.conf.d ...\n\n"
for i in $SCRIPT_DIR/wireplumber.conf.d/*; do
	if [[ ! -e /etc/wireplumber/wireplumber.conf.d/`basename $i` ]]; then
		printf "$RED`basename $i` is not installed...$RESET\n"
	else
		cmp --silent $i /etc/wireplumber/`basename $i` && printf "$GREEN`basename $i` is installed and up-to-date$RESET\n" || printf "$YELLOW`basename $i` is installed but out-of-date$RESET\n"
	fi
done

printf "\nChecking files in user directory /home/`whoami`/.config/wireplumber/wireplumber.conf.d ...\n\n"
for i in $SCRIPT_DIR/wireplumber.conf.d/*; do
	if [[ ! -e /home/`whoami`/.config/wireplumber/wireplumber.conf.d/`basename $i` ]]; then
		printf "$RED`basename $i` is not installed...$RESET\n"
	else
		cmp --silent $i /home/`whoami`/.config/wireplumber/wireplumber.conf.d/`basename $i` && printf "$GREEN`basename $i` is installed and up-to-date$RESET\n" || printf "$YELLOW`basename $i` is installed but out-of-date$RESET\n"
	fi
done