#/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

if [ `id -u` = 0 ]; then
	INSTALL_DIRECTORY="/etc/wireplumber"
	echo "Installing Wireplumber configs systemwide to $INSTALL_DIRECTORY..."
else
	INSTALL_DIRECTORY="/home/`whoami`/.config/wireplumber"
	echo "Installing Wireplumber configs into $INSTALL_DIRECTORY user directory..."
fi

mkdir -p $INSTALL_DIRECTORY/wireplumber.conf.d

ln -f $SCRIPT_DIR/wireplumber.conf.d/* $INSTALL_DIRECTORY/wireplumber.conf.d/

if [ `id -u` = 0 ]; then
	echo "Done, please restart Wireplumber manually..."
else
	echo "Done, restarting Wireplumber."
	systemctl --user restart wireplumber.service
fi